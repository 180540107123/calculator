package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BMIActivity extends BaseActivity {
    @BindView(R.id.tvBMIInput)
    TextView tvBMIInput;
    @BindView(R.id.tvBMIInput1)
    TextView tvBMIInput1;
    @BindView(R.id.etBMIInput2)
    EditText etBMIInput2;
    @BindView(R.id.tvBMIInput3)
    TextView tvBMIInput3;
    @BindView(R.id.etBMIInput4)
    EditText etBMIInput4;
    @BindView(R.id.tvBMIResult)
    TextView tvBMIResult;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bmiCalc)
    ImageView bmiCalc;
    @BindView(R.id.tvBMI)
    TextView tvBMI;
    @BindView(R.id.etBMI)
    EditText etBMI;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bmi);
        ButterKnife.bind(this);
        setUpActionBar("", true);

    }


    @OnClick(R.id.bmiCalc)
    public void onbmiCalcClicked() {

        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {

        double bmi;

        String w = etBMIInput4.getText().toString();
        String h = etBMIInput2.getText().toString();

        double weight = Double.valueOf(w).doubleValue();
        double height = Double.valueOf(h).doubleValue();


        bmi = (weight / ((height / 100) * (height / 100)));
        etBMI.setText(String.format(" %.2f", bmi));

    }
}





