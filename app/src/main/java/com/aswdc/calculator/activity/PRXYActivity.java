package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PRXYActivity extends BaseActivity {
    @BindView(R.id.tvPercentageofxfromyInput)
    TextView tvPercentageofxfromyInput;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.prxyCalc)
    ImageView prxyCalc;
    @BindView(R.id.tvvalueX)
    TextView tvvalueX;
    @BindView(R.id.etvalueX)
    EditText etvalueX;
    @BindView(R.id.tvvalueY)
    TextView tvvalueY;
    @BindView(R.id.etvalueY)
    EditText etvalueY;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvPercentageofxfromyResult)
    TextView tvPercentageofxfromyResult;
    @BindView(R.id.tvPercent)
    TextView tvPercent;
    @BindView(R.id.etPercent)
    EditText etPercent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.percentage_of_x_from_y);
        ButterKnife.bind(this);
        setUpActionBar("", true);

    }

    @OnClick(R.id.prxyCalc)
    public void onprxyCalcClicked() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {

        double value;

        String l = etvalueX.getText().toString();
        String w = etvalueY.getText().toString();

        double x = Double.valueOf(l).doubleValue();
        double y = Double.valueOf(w).doubleValue();

        value = (x / y) * 100;
        etPercent.setText(String.format(" %.2f ", value));
    }
}
