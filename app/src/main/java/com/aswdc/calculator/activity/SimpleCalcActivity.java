package com.aswdc.calculator.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SimpleCalcActivity extends BaseActivity {


    float Res1, Res2;
    boolean Add, Sub, Mul, Div, Per;
    String temp;

    @BindView(R.id.buttonAC)
    Button buttonAC;
    @BindView(R.id.buttonPM)
    Button buttonPM;
    @BindView(R.id.buttonPercentage)
    Button buttonPercentage;
    @BindView(R.id.buttondivision)
    Button buttondivision;
    @BindView(R.id.button7)
    Button button7;
    @BindView(R.id.button8)
    Button button8;
    @BindView(R.id.button9)
    Button button9;
    @BindView(R.id.button4)
    Button button4;
    @BindView(R.id.button5)
    Button button5;
    @BindView(R.id.button6)
    Button button6;
    @BindView(R.id.buttonMinus)
    Button buttonMinus;
    @BindView(R.id.button1)
    Button button1;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.buttonPlus)
    Button buttonPlus;
    @BindView(R.id.button0)
    Button button0;
    @BindView(R.id.buttonComma)
    Button buttonComma;
    @BindView(R.id.buttonEquqls)
    Button buttonEquqls;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_simple_calc)
    TextView tvSimpleCalc;
    @BindView(R.id.buttonmultiplication)
    Button buttonmultiplication;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_calc);
        ButterKnife.bind(this);
        setUpActionBar("Calculator", true);
    }

    @OnClick(R.id.button0)
    public void onButton0Clicked() {
        tvSimpleCalc.setText(temp + "" + 0);
    }

    @OnClick(R.id.button1)
    public void onButton1Clicked() {
        temp = (String) tvSimpleCalc.getText();
        tvSimpleCalc.setText(temp + "" + 1);
    }

    @OnClick(R.id.button2)
    public void onButton2Clicked() {
        temp = (String) tvSimpleCalc.getText();
        tvSimpleCalc.setText(temp + "" + 2);
    }

    @OnClick(R.id.button3)
    public void onButton3Clicked() {


        temp = (String) tvSimpleCalc.getText();
        tvSimpleCalc.setText(temp + "" + 3);
    }

    @OnClick(R.id.button4)
    public void onButton4Clicked() {

        temp = (String) tvSimpleCalc.getText();
        tvSimpleCalc.setText(temp + "" + 4);
    }

    @OnClick(R.id.button5)
    public void onButton5Clicked() {

        temp = (String) tvSimpleCalc.getText();
        tvSimpleCalc.setText(temp + "" + 5);
    }

    @OnClick(R.id.button6)
    public void onButton6Clicked() {
        temp = (String) tvSimpleCalc.getText();
        tvSimpleCalc.setText(temp + "" + 6);
    }

    @OnClick(R.id.button7)
    public void onButton7Clicked() {
        temp = (String) tvSimpleCalc.getText();
        tvSimpleCalc.setText(temp + "" + 7);
    }

    @OnClick(R.id.button8)
    public void onButton8Clicked() {
        temp = (String) tvSimpleCalc.getText();
        tvSimpleCalc.setText(temp + "" + 8);
    }

    @OnClick(R.id.button9)
    public void onButton9Clicked() {
        temp = (String) tvSimpleCalc.getText();
        tvSimpleCalc.setText(temp + "" + 9);
    }


    @OnClick(R.id.buttonAC)
    public void onButtonACClicked() {
        tvSimpleCalc.setText("");
    }

    @OnClick(R.id.buttonPlus)
    public void onButtonPlusClicked() {
        Log.d("String:::::", String.valueOf(tvSimpleCalc.getText()));
        if (tvSimpleCalc == null) {
            tvSimpleCalc.setText("");
        } else {
            Res1 = Float.parseFloat(tvSimpleCalc.getText() + "");
            Add = true;
            tvSimpleCalc.setText("+");
        }

    }

    @OnClick(R.id.buttonMinus)
    public void onButtonMinusClicked() {

        if (tvSimpleCalc == null) {
            tvSimpleCalc.setText("");
        } else {
            Res1 = Float.parseFloat(tvSimpleCalc.getText() + "");
            Sub = true;
            tvSimpleCalc.setText("-");
        }

    }

    @OnClick(R.id.buttonmultiplication)
    public void onViewClicked() {

        if (tvSimpleCalc == null) {
            tvSimpleCalc.setText("");
        } else {
            Res1 = Float.parseFloat(tvSimpleCalc.getText() + "");
            Mul = true;
            tvSimpleCalc.setText("*");
        }
    }

    @OnClick(R.id.buttondivision)
    public void onButtondivisionClicked() {

        if (tvSimpleCalc == null) {
            tvSimpleCalc.setText("");
        } else {
            Res1 = Float.parseFloat(tvSimpleCalc.getText() + "");
            Div = true;
            tvSimpleCalc.setText("/");
        }
    }

    @OnClick(R.id.buttonPM)
    public void onButtonPMClicked() {
        tvSimpleCalc.setText("-" + tvSimpleCalc.getText());
    }

    @OnClick(R.id.buttonPercentage)
    public void onButtonPercentageClicked() {

        if (tvSimpleCalc == null) {
            tvSimpleCalc.setText("");
        } else {
            Res1 = Float.parseFloat(tvSimpleCalc.getText() + "");
            Per = true;
            tvSimpleCalc.setText(null);
        }

    }

    @OnClick(R.id.buttonComma)
    public void onButtonCommaClicked() {
        //tvSimpleCalc.setText(".");
        Log.d("String:::::", String.valueOf(tvSimpleCalc.getText()));
        if (tvSimpleCalc == null) {
            tvSimpleCalc.setText(".");
        } else {
            Res1 = Float.parseFloat(tvSimpleCalc.getText() + "");
            Add = true;
            tvSimpleCalc.setText(null);
        }
    }


    @OnClick(R.id.buttonEquqls)
    public void onButtonEquqlsClicked() {
        Res2 = Float.parseFloat(tvSimpleCalc.getText() + "");
        if (Add == true) {
            tvSimpleCalc.setText(Res1 + Res2 + "");
            Add = false;
        }
        if (Sub == true) {
            tvSimpleCalc.setText(Res1 - Res2 + "");
            Sub = false;
        }
        if (Mul == true) {
            tvSimpleCalc.setText(Res1 * Res2 + "");
            Mul = false;
        }
        if (Div == true) {
            tvSimpleCalc.setText(Res1 / Res2 + "");
            Div = false;
        }
        if (Per == true) {
            tvSimpleCalc.setText(Res1 % Res2 + "");
            Per = false;
        }


    }


}
