package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IncreaseActivity extends BaseActivity {
    @BindView(R.id.tvIncreaseInput)
    TextView tvIncreaseInput;
    @BindView(R.id.tvIncreaseResult)
    TextView tvIncreaseResult;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.increaseCalc)
    ImageView increaseCalc;
    @BindView(R.id.tvInitial)
    TextView tvInitial;
    @BindView(R.id.etInitial)
    EditText etInitial;
    @BindView(R.id.tvIncrease)
    TextView tvIncrease;
    @BindView(R.id.etIncrease)
    EditText etIncrease;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvFinalValue)
    TextView tvFinalValue;
    @BindView(R.id.etFinalValue)
    EditText etFinalValue;
    @BindView(R.id.tvRaise)
    TextView tvRaise;
    @BindView(R.id.etRaise)
    EditText etRaise;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.increase);
        ButterKnife.bind(this);
        setUpActionBar("", true);
    }


    @OnClick(R.id.increaseCalc)
    public void onincreaseCalcClicked() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {

        double raise, finalvalue;

        String l = etInitial.getText().toString();
        String w = etIncrease.getText().toString();

        double initialvalue = Double.valueOf(l).doubleValue();
        double increase = Double.valueOf(w).doubleValue();

        raise = ((increase * initialvalue) / 100);
        etRaise.setText(String.format(" %.2f ", raise));
        finalvalue = (initialvalue + raise);
        etFinalValue.setText(String.format(" %.2f ", finalvalue));

    }
}
