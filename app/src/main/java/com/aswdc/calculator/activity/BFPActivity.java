package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BFPActivity extends BaseActivity {

    @BindView(R.id.tvBFPInput)
    TextView tvBFPInput;
    @BindView(R.id.tvBFPResult)
    TextView tvBFPResult;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bfpCalc)
    ImageView bfpCalc;
    @BindView(R.id.tvAge)
    TextView tvAge;
    @BindView(R.id.etAge)
    EditText etAge;
    @BindView(R.id.tvHeight)
    TextView tvHeight;
    @BindView(R.id.etHeight)
    EditText etHeight;
    @BindView(R.id.tvWeight)
    TextView tvWeight;
    @BindView(R.id.etWeight)
    EditText etWeight;
    @BindView(R.id.tvGender)
    TextView tvGender;
    @BindView(R.id.etGender)
    EditText etGender;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvBodyFat)
    TextView tvBodyFat;
    @BindView(R.id.etBodyFat)
    EditText etBodyFat;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bfp);
        ButterKnife.bind(this);
        setUpActionBar("", true);
    }


    @OnClick(R.id.bfpCalc)
    public void onbfpCalcClicked() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {

        double BodyFat, Bmi = 0;
        String gender;

//        int age = Integer.parseInt(etAge.getText().toString());
//        int weight = Integer.parseInt(etWeight.getText().toString());
//        int height = Integer.parseInt(etHeight.getText().toString());

             String Gender = etGender.getText().toString();
             String w = etWeight.getText().toString();
             String h = etHeight.getText().toString();
             String a = etAge.getText().toString();

        //String g = etGender.getText().toString();

        double weight = Double.valueOf(w).doubleValue();
        double height = Double.valueOf(h).doubleValue();
        double age = Double.valueOf(a).doubleValue();

       // double Gender = Double.valueOf(g).doubleValue();



        if (Gender.equals("Female") || Gender.equals("female"))
            {
                Bmi = (weight / ((height / 100) * (height / 100)));
               BodyFat =  ((1.20 * Bmi) + (0.23 * age) - 54);
                etBodyFat.setText(String.format(" %.2f", BodyFat)+"%");

            }
        else if(Gender.equals("Male") || Gender.equals("male"))
            {
                Bmi = (weight / ((height / 100) * (height / 100)));
                BodyFat =  ((1.20 * Bmi) + (0.23 * age) - 16.2);
                etBodyFat.setText(String.format(" %.2f", BodyFat)+"%");
            }
        else
        {
            etBodyFat.setText(String.format("Enter valid input."));
        }


    }

}
