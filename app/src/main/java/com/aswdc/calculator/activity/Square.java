package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Square extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvSquareInput)
    TextView tvSquareInput;
    @BindView(R.id.tvSide)
    TextView tvSide;
    @BindView(R.id.etSide)
    EditText etSide;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvSquareResult)
    TextView tvSquareResult;
    @BindView(R.id.tvArea)
    TextView tvArea;
    @BindView(R.id.etSArea)
    EditText etSArea;
    @BindView(R.id.tvPerimeter)
    TextView tvPerimeter;
    @BindView(R.id.etSPerimeter)
    EditText etSPerimeter;
    @BindView(R.id.SquareCalc)
    ImageView SquareCalc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.square);
        ButterKnife.bind(this);
        setUpActionBar("", true);


    }

    @OnClick(R.id.SquareCalc)
    public void onSquareCalcClicked() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {


        double area, perimeter;

        String l = etSide.getText().toString();

        double side = Double.valueOf(l).doubleValue();

        area = side * side;
        etSArea.setText(String.format(" %.2f ", area));
        perimeter = 4 * side;
        etSPerimeter.setText(String.format(" %.2f ", perimeter));
    }
}
