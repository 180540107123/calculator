package com.aswdc.calculator.activity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LcmGcf extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvValue)
    TextView tvValue;
    @BindView(R.id.tvValue1)
    TextView tvValue1;
    @BindView(R.id.etValue1)
    EditText etValue1;
    @BindView(R.id.tvValue2)
    TextView tvValue2;
    @BindView(R.id.etValue2)
    EditText etValue2;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvTimediffResult)
    TextView tvTimediffResult;
    @BindView(R.id.tvGreatestCommonFactor)
    TextView tvGreatestCommonFactor;
    @BindView(R.id.etGreatestCommonFactor)
    EditText etGreatestCommonFactor;
    @BindView(R.id.tvLeastCommonMultiple)
    TextView tvLeastCommonMultiple;
    @BindView(R.id.etLeastCommonMultiple)
    EditText etLeastCommonMultiple;
    @BindView(R.id.timediffCalc)
    ImageView timediffCalc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gcflcm);
        ButterKnife.bind(this);
        setUpActionBar("",true);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {
        int a,b, y, x,t,gcd,lcm;
        a=Integer.parseInt(etValue1.getText().toString());
        b=Integer.parseInt(etValue2.getText().toString());
        x=a;
        y=b;
        while(y!=0){
            t=y;
            y=x%y;
            x=t;
        }
        gcd = x;
        lcm=(a*b)/gcd;
        etGreatestCommonFactor.setText(""+gcd);
        etLeastCommonMultiple.setText(""+lcm);
    }
}
