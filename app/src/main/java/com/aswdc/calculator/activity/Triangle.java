package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Triangle extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTriangleInput)
    TextView tvTriangleInput;
    @BindView(R.id.tvsidea)
    TextView tvsidea;
    @BindView(R.id.etsidea)
    EditText etsidea;
    @BindView(R.id.tvsideb)
    TextView tvsideb;
    @BindView(R.id.etsideb)
    EditText etsideb;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvTriangleResult)
    TextView tvTriangleResult;
    @BindView(R.id.tvArea)
    TextView tvArea;
    @BindView(R.id.etArea)
    EditText etArea;
    @BindView(R.id.triangleCalc)
    ImageView triangleCalc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.triangle);
        ButterKnife.bind(this);
        setUpActionBar("", true);
    }

    @OnClick(R.id.triangleCalc)
    public void onLinearlayoutBFIClicked() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {

        double area, circumference;

        String l = etsidea.getText().toString();
        String w = etsideb.getText().toString();

        double h = Double.valueOf(l).doubleValue();
        double b = Double.valueOf(w).doubleValue();


        area = 0.5 * h * b;
        etArea.setText(String.format(" %.2f ", area));


    }
}
