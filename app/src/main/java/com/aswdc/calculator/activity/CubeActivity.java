package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CubeActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvAddSubTimeStartDate)
    TextView tvAddSubTimeStartDate;
    @BindView(R.id.addsubtimeCalc)
    ImageView addsubtimeCalc;
    @BindView(R.id.tvSide)
    TextView tvSide;
    @BindView(R.id.etSide)
    EditText etSide;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvArea)
    TextView tvArea;
    @BindView(R.id.etArea)
    EditText etArea;
    @BindView(R.id.tvVolume)
    TextView tvVolume;
    @BindView(R.id.etVolume)
    EditText etVolume;
    @BindView(R.id.tvPerimeter)
    TextView tvPerimeter;
    @BindView(R.id.etPerimeter)
    EditText etPerimeter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cube);
        ButterKnife.bind(this);
        setUpActionBar("", true);
    }

    @OnClick(R.id.addsubtimeCalc)
    public void onaddsubtimeCalclicked() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {


        double area, perimeter, volume;

        String a = etSide.getText().toString();

        double side = Double.valueOf(a).doubleValue();

        area = 6 * side * side;
        etArea.setText(String.format(" %.2f ", area));
        volume = side * side * side;
        etVolume.setText(String.format(" %.2f ", volume));
        perimeter = 12 * side;
        etPerimeter.setText(String.format(" %.2f ", perimeter));
    }
}
