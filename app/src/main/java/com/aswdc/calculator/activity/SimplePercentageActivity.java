package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SimplePercentageActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.SimplePerCalc)
    ImageView SimplePerCalc;
    @BindView(R.id.tvSimplePercentageInput)
    TextView tvSimplePercentageInput;
    @BindView(R.id.tvPercent)
    TextView tvPercent;
    @BindView(R.id.etPercent)
    EditText etPercent;
    @BindView(R.id.tvFrom)
    TextView tvFrom;
    @BindView(R.id.etFrom)
    EditText etFrom;
    @BindView(R.id.tvSimplePercentageResult)
    TextView tvSimplePercentageResult;
    @BindView(R.id.tvValue)
    TextView tvValue;
    @BindView(R.id.etValue)
    EditText etValue;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_percentage);
        ButterKnife.bind(this);
        setUpActionBar("", true);
    }


    @OnClick(R.id.SimplePerCalc)
    public void onSimplePerCalcClicked() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {


        double value;

        String l = etPercent.getText().toString();
        String w = etFrom.getText().toString();

        double from = Double.valueOf(l).doubleValue();
        double per = Double.valueOf(w).doubleValue();


        value = (per / 100) * from;
        etValue.setText(String.format(" %.2f ", value));

    }
}
