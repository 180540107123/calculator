package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class IncreaseDecreaseActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvAddSubTimeStartDate)
    TextView tvAddSubTimeStartDate;
    @BindView(R.id.tvfrom)
    TextView tvfrom;
    @BindView(R.id.etValue1)
    EditText etValue1;
    @BindView(R.id.tvto)
    TextView tvto;
    @BindView(R.id.etValue2)
    EditText etValue2;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvAddSubDateAddSubtract)
    TextView tvAddSubDateAddSubtract;
    @BindView(R.id.tvPercent)
    TextView tvPercent;
    @BindView(R.id.etPercent)
    EditText etPercent;
    @BindView(R.id.AddSubDateCalc)
    ImageView AddSubDateCalc;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.increasedecrease);
        ButterKnife.bind(this);
        setUpActionBar("", true);
    }


    @OnClick(R.id.AddSubDateCalc)
    public void onAddSubDateCalcClicked() {

        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {

        double percentage;

        String l = etValue1.getText().toString();
        String w = etValue2.getText().toString();

        double from = Double.valueOf(l).doubleValue();
        double to = Double.valueOf(w).doubleValue();


        percentage = (((to * 100) / from) - 100);
        etPercent.setText(String.format(" %.2f  ", percentage) + "%");


    }
}