package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PrimeCheckerActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvDateDiffStartTime)
    TextView tvDateDiffStartTime;
    @BindView(R.id.tvValue)
    TextView tvValue;
    @BindView(R.id.etValue)
    EditText etValue;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvDateDiffResult)
    TextView tvDateDiffResult;
    @BindView(R.id.tvPrime)
    TextView tvPrime;
    @BindView(R.id.etPrime)
    EditText etPrime;
    @BindView(R.id.tvNextPrime)
    TextView tvNextPrime;
    @BindView(R.id.etNextPrime)
    EditText etNextPrime;
    @BindView(R.id.datediffCalc)
    ImageView datediffCalc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.primechecker);
        ButterKnife.bind(this);
        setUpActionBar("", true);

    }

    @OnClick(R.id.datediffCalc)
    public void ondatediffCalcClicked() {

        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {

        int num = Integer.parseInt(etValue.getText().toString());

        // prime
        boolean flag = false;
        for (int i = 2; i <= num / 2; ++i) {
            if (num % i == 0) {
                flag = true;
                break;
            }
        }

        if (!flag)
            etPrime.setText("Yes");
        else
            etPrime.setText("No");
    }

<<<<<<<<< Temporary merge branch 1
        //next prime


        int prime = 0;

        for (int j = num; j < 15; j++) {

            int count = 0;
            for (int i = 2; i <= j / 2; i++) {

                if (j % i == 0) {
                    count++;
                }
            }

            if (count == 0) {

                prime = j;
                break;

            }

        }

        etNextPrime.setText("" + prime);


    }

=========
    //next prime

//    int num = Integer.parseInt(etValue.getText().toString());
//
//    int num = 8;
//    int prime = 0;
//
//            for(int j = num; j<15; j++){
//
//        int count = 0;
//        for(int i=2; i<=j/2; i++){
//
//            if(j%i==0){
//                count++;
//            }
//        }
//
//        if(count==0){
//
//            prime = j;   //assign next prime
//            break;
//
//        }
//
//    }
//
//
//   etNextPrime.setText(""+prime);
>>>>>>>>> Temporary merge branch 2

}


