package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CircleActivity extends BaseActivity {
    @BindView(R.id.tvCircleInput)
    TextView tvCircleInput;
    @BindView(R.id.tvCircleResult)
    TextView tvCircleResult;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.circleCalc)
    ImageView circleCalc;
    @BindView(R.id.tvRadius)
    TextView tvRadius;
    @BindView(R.id.etradius)
    EditText etradius;
    @BindView(R.id.tvDiameter)
    TextView tvDiameter;
    @BindView(R.id.etDiameter)
    EditText etDiameter;
    @BindView(R.id.tvArea)
    TextView tvArea;
    @BindView(R.id.etArea)
    EditText etArea;
    @BindView(R.id.tvCircumference)
    TextView tvCircumference;
    @BindView(R.id.etCircumference)
    EditText etCircumference;

    @BindView(R.id.tvCalculate)
    TextView tvCalculate;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.circle);
        ButterKnife.bind(this);
        setUpActionBar("", true);


    }


    @OnClick(R.id.circleCalc)
    public void oncircleCalcClicked() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {

        double area, diameter, circumference, pi = 3.14;

        String radius = etradius.getText().toString();

        double r = Double.valueOf(radius).doubleValue();

        area = pi * r * r;
        etArea.setText(String.format(" %.2f ", area));
        circumference = 2 * pi * r;
        etCircumference.setText(String.format(" %.2f ", circumference));
        diameter = 2 * r;
        etDiameter.setText(String.format(" %.2f ", diameter));

    }


}
