package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.tvActAllCalculator)
    TextView tvActAllCalculator;
    @BindView(R.id.tvActFavorites)
    TextView tvActFavorites;
    @BindView(R.id.tvActIncrese)
    TextView tvActIncrese;
    @BindView(R.id.tvActDiscount)
    TextView tvActDiscount;
    @BindView(R.id.tvActSimplePercentage)
    TextView tvActSimplePercentage;
    @BindView(R.id.tvActPercentagexy)
    TextView tvActPercentagexy;
    @BindView(R.id.tvActRectangle)
    TextView tvActRectangle;
    @BindView(R.id.tvActSquare)
    TextView tvActSquare;
    @BindView(R.id.tvActTriangle)
    TextView tvActTriangle;
    @BindView(R.id.tvActCircle)
    TextView tvActCircle;
    @BindView(R.id.tvActBodyFatIndex)
    TextView tvActBodyFatIndex;
    @BindView(R.id.tvActBodyMassIndex)
    TextView tvActBodyMassIndex;
    @BindView(R.id.btnActCalculator)
    Button btnActCalculator;
    @BindView(R.id.linearlayoutTimeDifference)
    LinearLayout linearlayoutTimeDifference;
    @BindView(R.id.linearlayoutTimeAddSub)
    LinearLayout linearlayoutTimeAddSub;
    @BindView(R.id.linearlayoutDateDifference)
    LinearLayout linearlayoutDateDifference;
    @BindView(R.id.linearlayoutDateAddSub)
    LinearLayout linearlayoutDateAddSub;
    @BindView(R.id.linearlayoutIncrease)
    LinearLayout linearlayoutIncrease;
    @BindView(R.id.linearlayoutDiscount)
    LinearLayout linearlayoutDiscount;
    @BindView(R.id.linearlayoutSimplePercentage)
    LinearLayout linearlayoutSimplePercentage;
    @BindView(R.id.linearlayoutPercentageXY)
    LinearLayout linearlayoutPercentageXY;
    @BindView(R.id.linearlayoutRectangle)
    LinearLayout linearlayoutRectangle;
    @BindView(R.id.linearlayoutSquare)
    LinearLayout linearlayoutSquare;
    @BindView(R.id.linearlayoutTriangle)
    LinearLayout linearlayoutTriangle;
    @BindView(R.id.linearlayoutCircle)
    LinearLayout linearlayoutCircle;
    @BindView(R.id.linearlayoutBFI)
    LinearLayout linearlayoutBFI;
    @BindView(R.id.linearlayoutBMI)
    LinearLayout linearlayoutBMI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.tvActAllCalculator)
    public void onTvActAllCalculatorClicked() {
    }


    @OnClick(R.id.btnActCalculator)
    public void onBtnActCalculatorClicked() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);

    }


    @OnClick(R.id.linearlayoutTimeDifference)
    public void onLinearlayoutTimeDifferenceClicked() {
        Intent intent = new Intent(this, LcmGcf.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutTimeAddSub)
    public void onLinearlayoutTimeAddSubClicked() {
        Intent intent = new Intent(this, CubeActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutDateDifference)
    public void onLinearlayoutDateDifferenceClicked() {

        Intent intent = new Intent(this, PrimeCheckerActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutDateAddSub)
    public void onLinearlayoutDateAddSubClicked() {

        Intent intent = new Intent(this, IncreaseDecreaseActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutIncrease)
    public void onLinearlayoutIncreaseClicked() {

        Intent intent = new Intent(this, IncreaseActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutDiscount)
    public void onLinearlayoutDiscountClicked() {

        Intent intent = new Intent(this, DiscountActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutSimplePercentage)
    public void onLinearlayoutSimplePercentageClicked() {

        Intent intent = new Intent(this, SimplePercentageActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutPercentageXY)
    public void onLinearlayoutPercentageXYClicked() {

        Intent intent = new Intent(this, PRXYActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutRectangle)
    public void onLinearlayoutRectangleClicked() {

        Intent intent = new Intent(this, RectangleActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutSquare)
    public void onLinearlayoutSquareClicked() {
        Intent intent = new Intent(this, Square.class);
        startActivity(intent);
        Log.d("::1::", "excute");
    }

    @OnClick(R.id.linearlayoutTriangle)
    public void onLinearlayoutTriangleClicked() {
        Intent intent = new Intent(this, Triangle.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutCircle)
    public void onLinearlayoutCircleClicked() {

        Intent intent = new Intent(this, CircleActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutBFI)
    public void onLinearlayoutBFIClicked() {

        Intent intent = new Intent(this, BFPActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.linearlayoutBMI)
    public void onLinearlayoutBMIClicked() {

        Intent intent = new Intent(this, BMIActivity.class);
        startActivity(intent);
    }
}

