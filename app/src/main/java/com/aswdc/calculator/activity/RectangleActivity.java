package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RectangleActivity extends BaseActivity {
    @BindView(R.id.tvRectangleInput)
    TextView tvRectangleInput;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rectangleCalc)
    ImageView rectangleCalc;
    @BindView(R.id.tvLength)
    TextView tvLength;
    @BindView(R.id.etLength)
    EditText etLength;
    @BindView(R.id.tvWidth)
    TextView tvWidth;
    @BindView(R.id.etWidth)
    EditText etWidth;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvArea)
    TextView tvArea;
    @BindView(R.id.etRArea)
    EditText etRArea;
    @BindView(R.id.tvPerimeter)
    TextView tvPerimeter;
    @BindView(R.id.etRPerimeter)
    EditText etRPerimeter;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.rectangle);
            ButterKnife.bind(this);
            setUpActionBar("", true);

        }


    @OnClick(R.id.rectangleCalc)
    public void rectangleCalc() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {

        double area, perimetere;

        String l = etLength.getText().toString();
        String w = etWidth.getText().toString();

        double length = Double.valueOf(l).doubleValue();
        double width = Double.valueOf(w).doubleValue();

        area = length * width;
        etRArea.setText(String.format(" %.2f ", area));
        perimetere = 2 * (length + width);
        etRPerimeter.setText(String.format(" %.2f ", perimetere));

    }
}
