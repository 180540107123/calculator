package com.aswdc.calculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DiscountActivity extends BaseActivity {
    @BindView(R.id.tvDiscountInput)
    TextView tvDiscountInput;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.discountCalc)
    ImageView discountCalc;
    @BindView(R.id.tvInitialValue)
    TextView tvInitialValue;
    @BindView(R.id.etInitialValue)
    EditText etInitialValue;
    @BindView(R.id.tvDiscount)
    TextView tvDiscount;
    @BindView(R.id.etDiscount)
    EditText etDiscount;
    @BindView(R.id.tvCalculate)
    TextView tvCalculate;
    @BindView(R.id.tvFinalValue)
    TextView tvFinalValue;
    @BindView(R.id.etFinalValue)
    EditText etFinalValue;
    @BindView(R.id.tvSaving)
    TextView tvSaving;
    @BindView(R.id.etSaving)
    EditText etSaving;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discount);
        ButterKnife.bind(this);
        setUpActionBar("", true);
    }


    @OnClick(R.id.discountCalc)
    public void ondiscountCalcClicked() {
        Intent intent = new Intent(this, SimpleCalcActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvCalculate)
    public void onViewClicked() {

        double finalvalue, saving;

        String l = etInitialValue.getText().toString();
        String w = etDiscount.getText().toString();

        double initialvalue = Double.valueOf(l).doubleValue();
        double discount = Double.valueOf(w).doubleValue();

        saving = (discount / 100) * initialvalue;
        etSaving.setText(String.format(" %.2f ", saving));
        finalvalue = initialvalue - saving;
        etFinalValue.setText(String.format(" %.2f ", finalvalue));
    }
}
